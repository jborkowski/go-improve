package main

import (
	"fmt"
	"math"
)

type Abser interface {
	Abs() float64
}

type Vertex struct {
	x, y float64
}

func (v *Vertex) Scale(f float64) {
	v.x = v.x * f
	v.y = v.y * f
}

func (v *Vertex) Abs() float64 {
	return math.Sqrt(v.x*v.x + v.y*v.y)
}

func main() {
	v := Vertex{10.23, 31.34}
	fmt.Println(v)
	v.Scale(2.34)
	fmt.Println(v)

	var i interface{} = "HOO HOO"
	s, ok := i.(string)
	if ok {
		fmt.Println(s)
	}
}
